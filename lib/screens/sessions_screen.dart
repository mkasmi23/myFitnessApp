import 'package:flutter/material.dart';
import 'package:helloworld/data/session.dart';
import 'package:helloworld/data/sp_helper.dart';

class SessionsScreen extends StatefulWidget {
  const SessionsScreen({Key? key}) : super(key: key);
  _SessionsScreenState createState() => _SessionsScreenState();
}

class _SessionsScreenState extends State<SessionsScreen> {
  TextEditingController txtDescription = TextEditingController();
  TextEditingController txtDuration = TextEditingController();
  final SpHelper spHelper = new SpHelper();
  List<Session> sessions = [];
  @override
  void initState() {
    // TODO: implement initState
    spHelper.init().then((value) => updateScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Your trainig session'),
        ),
        body: ListView(children: getContent()),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              showSessionDialog(context);
            }));
  }

  Future<dynamic> showSessionDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('insert trainig session'),
              content: SingleChildScrollView(
                child: Column(children: [
                  TextField(controller: txtDescription, decoration: InputDecoration(hintText: 'Description')),
                  TextField(controller: txtDuration, decoration: InputDecoration(hintText: 'Duration')),
                ]),
              ),
              actions: [
                TextButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      txtDescription.text = '';
                      txtDuration.text = '';
                      Navigator.of(context).pop();
                    }),
                ElevatedButton(
                  child: Text('save'),
                  onPressed: saveSession,
                ),
              ]);
        });
  }

  Future saveSession() async {
    DateTime now = DateTime.now();
    String today = '${now.year}-${now.month}-${now.day}';
    int id = spHelper.getCounter() + 1;
    Session newSession = Session(1, today, txtDescription.text, int.tryParse(txtDuration.text) ?? 0);
    spHelper.writeSession(newSession).then((value) {
      updateScreen();
      spHelper.setCounter();
    });
    txtDescription.text = '';
    txtDuration.text = '';
    return Navigator.of(context).pop();
  }

  List<Widget> getContent() {
    List<Widget> tiles = [];
    sessions.forEach((session) {
      tiles.add(Dismissible(
        key: UniqueKey(),
        onDismissed: (_) {
          spHelper.deleteSession(session.id).then((value) => updateScreen());
        },
        child: ListTile(
          title: Text(session.description),
          subtitle: Text('${session.date} - duration: ${session.duration} min'),
        ),
      ));
    });
    return tiles;
  }

  void updateScreen() {
    sessions = spHelper.getSessions();
    setState(() {});
  }
}
