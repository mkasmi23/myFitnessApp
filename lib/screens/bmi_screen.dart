import 'package:flutter/material.dart';
import 'package:helloworld/shared/menu_bottom.dart';
import 'package:helloworld/shared/menu_drawer.dart';

class BmiScreen extends StatefulWidget {
  const BmiScreen({Key? key}) : super(key: key);

  @override
  State<BmiScreen> createState() => _BmiScreenState();
}

class _BmiScreenState extends State<BmiScreen> {
  String result = '';
  bool isMetric = true;
  bool isEmperial = false;
  double? height; //to make theses variables nullables, to avoid non safety of the vakues of vaiables;//to make theses variables nullables, to avoid non safety of the vakues of vaiables
  double? weight;
  late List<bool> isSelected;
  String heightMessage = '';
  String weightMessage = '';
  TextEditingController txtHeight = TextEditingController();
  TextEditingController txtWeight = TextEditingController();

  @override
  void initState() {
    isSelected = [
      isMetric,
      isEmperial
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    heightMessage = 'Please insert your height in' + ((isMetric) ? ' meters' : ' inches');
    weightMessage = 'Please insert your weight in' + ((isMetric) ? ' kilos' : ' pounds');
    return Scaffold(
        appBar: AppBar(
          title: Text('BMI Calculator'),
        ),
        bottomNavigationBar: MenuBottom(),
        drawer: MenuDrawer(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              ToggleButtons(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text('Metric'),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text('Imperial'),
                  ),
                ],
                isSelected: isSelected,
                onPressed: toggleMeasure,
              ),
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: TextField(
                  controller: txtHeight,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(hintText: heightMessage),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: TextField(controller: txtWeight, keyboardType: TextInputType.number, decoration: InputDecoration(hintText: weightMessage)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  child: const Text('Calculate BMI'),
                  onPressed: findBMI,
                ),
              ),
              Text('your BMI is:$result')
            ],
          ),
        ));
  }

  toggleMeasure(value) {
    if (value == 0) {
      isMetric = true;
      isEmperial = false;
    } else {
      isMetric = false;
      isEmperial = true;
    }
    setState(() {
      isSelected = [
        isMetric,
        isEmperial
      ];
    });
  }

  void findBMI() {
    double bmi = 0;
    double height = double.tryParse(txtWeight.text) ?? 0;
    double weight = double.tryParse(txtWeight.text) ?? 0;
    if (isMetric) {
      bmi = weight / (height * weight);
    } else {
      bmi = weight * 703 / (height * weight);
    }
    setState(() {
      result = 'Your BMI is' + bmi.toString();
    });
  }
}
