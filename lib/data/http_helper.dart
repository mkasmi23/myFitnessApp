import 'package:helloworld/data/weather.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HttpHelper {
  //https://api.openweathermap.org/data/2.5/weather?q=London&appid=3e78044b9287afe65b425559d0acc830

  final authority = 'api.openweathermap.org';
  final String path = 'data/2.5/weather';
  final String apiKey = '3e78044b9287afe65b425559d0acc830';

  Future<Weather> getWeather(String location) async {
    Map<String, dynamic> parameters = {
      'q': location,
      'appid': apiKey
    };
    Uri uri = Uri.https(authority, path, parameters);
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(response.body);
    Weather weather = Weather.fromJson(data);
    return weather;
  }
}
