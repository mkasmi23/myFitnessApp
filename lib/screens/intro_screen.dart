import 'package:flutter/material.dart';
import 'package:helloworld/shared/menu_bottom.dart';
import 'package:helloworld/shared/menu_drawer.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Globo fitness'),
      ),
      drawer: MenuDrawer(),
      bottomNavigationBar: MenuBottom(),
      body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/beach.png'), fit: BoxFit.cover)),
          child: Center(
            child: Container(
                padding: EdgeInsets.all(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16.0),
                  color: Colors.green,
                ),
                child: Text('my first Flutter application !')),
          )),
    );
  }
}
